<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원 일괄 등록</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       	<div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사용자 일괄 등록</div>                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<p>신규 등록할 사용자 정보를 엑셀파일(CSV)로 업로드 하여, 최대 100건까지 일괄 등록할 수 있습니다.<br>
							등록 양식 샘플을 다운로드 받아, 신규 구성원 정보를 등록하세요.&nbsp;&nbsp;<a href="#" class="sample_download">샘플 다운로드</a><br>
							양식에 맞지 않거나, 유효하지 않은 정보는 빨간 글씨로 표시됩니다. 이 경우 상단 메뉴에서 선택 또는 리스트에서 직접 수정한 후 등록하세요.<br>
							관리자가 설정한 비밀번호는 임시비밀번호이며, 사용자가 직접 비밀번호를 변경한 후 이용할 수 있습니다.<br></p>
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit">파일 선택</div>
                        	<div class="main_cnt_desc">
                        		<input type="file">
                        	</div>
                        	<div class="main_cnt_desc">
                        		<button class="btn_main" id="find_file_btn" onclick="tbl_show();">찾기</button>
                        	</div>
                        	<div class="accept_btn">
                        		<button>적용</button>
                        	</div>
                        </div>                       
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic" id="file_table">                  	
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 16%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>이름</th>
                                    <th>이메일</th>
                                    <th>입사일</th>
                                    <th>부서</th>
                                    <th>직급</th>
                                    <th>직책</th>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="../ajax/ajax.jsp" class="modal_pop" rel="modal:open">팝업예시</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">내용1</a></td class="tit">
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
       	
        $("#file_table").hide();
  
    });
    
    function tbl_show(){
    	$("#file_table").show();
    }
    
    
</script>
</body>
</html>