<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 게시물 작성
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>공지사항 작성</h2></div>

                    
                   <form action="" encType="multipart/form-data">
	                    <div id="btnSet">
	                    	<a href="#popex" rel="modal:open" class="button btn_white">임시저장함</a>
		                    <button type="button" class="btn_white">임시저장</button>
	                    </div>
                    	<div id="check">
                        	<input type="checkbox" id="mustRead">
                        	<label for="mustRead">필독 여부</label>
                        </div>
                        
                        <div id="kinds">
	                        <label>게시판 선택</label>
	                        <select name="" id="">
	                            <option value="com">전체</option>
	                            <option value="dept">팀</option>
	                        </select>
                        </div>
						
						<div id="title">
	                        <label for="">제목</label>
	                        <input type="text" name="title">
                        </div>
                        
                        <!-- 에디터 -->
						<div class="fr-view"></div>
 						
 						<!-- 첨부파일 -->
	 						<div class="attachment">
		 						첨부파일
		 						<button type="button" class="btn_solid_pink addAction" onclick="addAtt(this)"><i class="fas fa-plus"></i></button>
		 					</div>
		 						<!--input box-->
		 						<div class="attActions">
		 							<div class="attAction">
										<input type="text" class="upload_text" readonly="readonly">
										<!--button-->
										<div class="upload-btn_wrap">
										  <button type="button" class="btn_main btn_choise"><input type="file" class="input_file">선택</button>
										  <button type="button" class="btn_white btn_delete">삭제</button>
										</div>
									</div>
								</div>
							<div class="endBtnSet">
		                        <button type="button" class="btn_pink btn_cancel">취소</button>
								<button type="submit" class="btn_main btn_add" onclick="return boardAdd()">등록</button>
							</div>
                    </form>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

    $(function(){
    	var editor = new FroalaEditor('.fr-view')
    	
    	$('.trumbowyg-fullscreen-button').remove();
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
/*         $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on"); */


        
        // 첫 첨부파일은 삭제불가
        $('.attAction:nth-of-type(1) .btn_delete').hide();
        
        function deleteAtt(val) {
     		console.log(1);
     	}
        
        $('.fr-quick-insert').remove();
	});
    
 	// 파일 업로드시 파일이름이 보여짐
    $(document).on("change",".input_file",function(event){
    	var i = $(this).val();
		$(this).parents('.attAction').children('.upload_text').val(i);
    });
 	
 	// 첨부파일 삭제
    $(document).on("click",".btn_delete",function(event){
    	$(this).parents('.attAction').remove();
    });
    
    // 첨부파일 추가
    function addAtt() {
    	let attActionClone = '<div class="attAction">';
			attActionClone += '<input type="text" class="upload_text" readonly="readonly">'
			attActionClone += '<div class="upload-btn_wrap">'
			attActionClone += '<button class="btn_main btn_choise"><input type="file" class="input_file">선택</button>'
			attActionClone += '<button type="button" class="btn_white btn_delete" >삭제</button>'
			attActionClone += '</div></div>';
		

		$('.attActions').append(attActionClone);
		// 두번째부터 삭제버튼 생성
		$('.attAction:nth-of-type(n+2) .btn_delete').show();
 	}

    function boardAdd() {
    	if($('#title>input').val() === '' || $('.fr-view').html() === '' || $('.fr-view').html() === '<p><br></p>') {
    		alert('작성하지 않은 항목이 있습니다.');
    		return false;
		} else {
			if(confirm('이대로 등록하시겠습니까?')) {
				return true;	
			} else {
				return false;
			}
		}
    }
</script>
</body>
</html>
