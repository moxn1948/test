<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
    
<h3 class="main_tit">문서함</h3>

    	<ul class="eas_tabs" id="eas_pop_tabs">
			<li class="eas_tab-link current" data-tab="eas_tab-1">완료함</li>
			<li class="eas_tab-link" data-tab="eas_tab-2">공람완료</li>							
		</ul>
		<!-- 완료함 시작 -->
		<div class="eas_tab-content current" id="eas_tab-1">
			<div class="eas_pop_top">
				<div class="eas_pop_top1">
					<div class="eas_pop_top_tit">완료함 (23)</div>
					<div class="eas_pop_top_select">년도<select><option>2020</option></select></div>
				</div>
				<div class="eas_pop_top2">
					<div>
						문서번호 <input type="text" class="pop_input">
						제목 <input type="text" class="pop_input">
						기안자 <i class="far fa-user"></i><input type="text" class="pop_input">
						등록일자 <input type="text" name="" id="datepicker" class="datepicker pop_input">
					</div>
					<div class="eas_pop_top_btn">
						<button class="btn_solid_pink">검색</button>
					</div>					
				</div>
			</div>
		
		<div class="eas_pop_padding_div">
			<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic eas_pop_tbl">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 5%;">
                                    <col style="width: 25%;">
                                    <col style="width: 40%;">
                                    <col style="width: 15%;">
                                    <col style="width: 15%;">
                                </colgroup>
                                <thead class="eas_pop_tbl_head">
                                <tr class="tbl_main_tit">
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>문서번호</th>
                                    <th>제목</th>
                                    <th>기안(접수)자</th>
                                    <th>결재일</th>
                                </tr>
                                </thead>
                                <% for(int i=0; i<15; i++) { %>
                                <tr>
                                	<td><input type="checkbox"></td>
                                	<td>값이 들어가면?</td>
                                	<td></td>
                                	<td></td>
                                	<td></td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="eas_pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                    
                    <div class="eas_pop_btn_div">
                    	<button class="btn_white">취소</button>
                    	<button class="btn_main">확인</button>
                    </div>
		</div>
		</div>
		<!-- 완료함 끝  -->
		<!-- 공람완료 시작 -->
		<div class="eas_tab-content" id="eas_tab-2">
			<div class="eas_pop_top">
				<div class="eas_pop_top1">
					<div class="eas_pop_top_tit">공람완료 (20)</div>
				</div>
				<div class="eas_pop_top2">
					<div>
						문서번호 <input type="text" class="pop_input">
						제목 <input type="text" class="pop_input">
						기안자 <i class="far fa-user"></i><input type="text" class="pop_input">
						등록일자 <input type="text" name="" id="datepicker" class="datepicker pop_input">
					</div>
					<div class="eas_pop_top_btn">
						<button class="btn_solid_pink">검색</button>
					</div>					
				</div>
			</div>
		
		<div class="eas_pop_padding_div">
			<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic eas_pop_tbl">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 5%;">
                                    <col style="width: 25%;">
                                    <col style="width: 40%;">
                                    <col style="width: 15%;">
                                    <col style="width: 15%;">
                                </colgroup>
                                <thead class="eas_pop_tbl_head">
                                <tr class="tbl_main_tit">
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>문서번호</th>
                                    <th>제목</th>
                                    <th>기안(접수)자</th>
                                    <th>결재일</th>
                                </tr>
                                </thead>
                                <% for(int i=0; i<15; i++) { %>
                                <tr>
                                	<td><input type="checkbox"></td>
                                	<td>값이 들어가면?</td>
                                	<td></td>
                                	<td></td>
                                	<td></td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="eas_pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                    
                    <div class="eas_pop_btn_div">
                    	<button class="btn_white">취소</button>
                    	<button class="btn_main">확인</button>
                    </div>
		</div>
		</div>
		<!-- 공람완료 끝 -->
		

<script>
//탭 전환용 스크립트
$(document).ready(function(){
	
	$('ul.eas_tabs li').click(function(){
		console.log(this);
		var tab_id = $(this).attr('data-tab');
		console.log(tab_id);

		$('ul.eas_tabs li').removeClass('current');
		$('.eas_tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});
});

	

$("#datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    nextText: '다음 달',
    prevText: '이전 달',
    dateFormat: "yy/mm/dd",
    showMonthAfterYear: true , 
    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
});
</script>


<!-- <button class="btn_main"><a href="#" rel="modal:close">Close</a></button> -->
