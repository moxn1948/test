<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/eas_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>참여 결재 - 결재진행</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <select name="" id="">
                            <option value="">제목</option>
                            <option value="">기안자</option>
                        </select>
                        <input type="search" name="" id="">
                        <button class="btn_solid_main">검색</button>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 8%;">
                                    <col style="width: 12%;">
                                    <col style="width: 15%;">
                                    <col style="width: 20%;">
                                    <col style="width: 5%;">
                                    <col style="width: 10%;">
                                    <col style="width: 10%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>상태</th>
                                    <th>서식명</th>
                                    <th>문서번호</th>
                                    <th>제목</th>
                                    <th>처리구분</th>
                                    <th>기안자</th>
                                    <th>기안 일시</th>                        
                                </tr>
                                <% for(int i=0; i<15; i++) { %>
                                <tr>
                                    <td><a href="#">내용5</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        var path = '${ contextPath }';

        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on");

        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
    });

</script>
</body>
</html>