<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 부재설정-부재이력 페이지
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>부재설정</h2></div>
                    
                    <!-- 년도 선택 -->
                    <div class="add_area">
                    	<button class="btn_main abs_add">추가</button>
                    </div>

                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap abs_table">
                            <table class="tbl_ctn">
                                <tr>
                                	<th>시작일</th>
                                	<th>종료일</th>
                                	<th>사유</th>
                                	<th>대리결재자</th>
                                	<th>결재이력</th>
                                </tr>
                                <!-- 없을 시 한줄로 "설정한 부재가 없습니다." 표기 -->
                                
	                            <c:forEach var="i" begin="0" end="13" step="1">
	                                <tr>
	                                	<td>20/01/01</td>
	                                	<td>20/01/01</td>
	                                	<td>특별휴가</td>
	                                	<td><a href="#popDetailAbsence" rel="modal:open">이원경</a></td>
	                                	<td>
	                                		<a href="#popListAbsence" rel="modal:open">
	                                			<button class="btn_main">결재이력</button>
	                                		</a>
	                                	</td>
	                                </tr>
	                            </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 결재이력상세보기 팝업 -->
<div id="popListAbsence" class="modal">
	<jsp:include page="mypage_pop_list_abs.jsp" />
</div>

<!-- 대결자 상세 팝업 -->
<div id="popDetailAbsence" class="modal">
	<jsp:include page="mypage_pop_detail_abs.jsp" />
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
    });
</script>
</body>
</html>
