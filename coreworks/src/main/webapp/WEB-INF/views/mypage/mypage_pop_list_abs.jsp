<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">결재이력 상세보기</h3>

                    <div class="tbl_common tbl_basic absence_status">
                    	<div class="tbl_wrap">
                            <table class="tbl_ctn">
                            	<colgroup>
                                    <col style="width: *;">
                                    <col style="width: 40%;">
                                    <col style="width: *;">
                                    <col style="width: *;">
                                </colgroup>
	                                <tr>
										<th>문서번호</th>
										<th>제목</th>
										<th>기안자</th>
										<th>결재일시</th>
	                                </tr>
	                                <tr>
	                                	<td>구매이력-2020-10</td>
	                                	<td>뱁새 사료대금</td>
	                                	<td>조문정</td>
	                                	<td>20/01/01</td>
	                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="close_area">
                    	<button class="btn_main detail_close"><a href="#" rel="modal:close">확인</a></button>
                    </div>