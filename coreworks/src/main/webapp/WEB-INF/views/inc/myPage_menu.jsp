<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:include page="./header.jsp" />
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div id="menu_area">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="#">내 연차 현황</a></li>
                        <li class="menu_list"><a href="#">부재설정</a></li>
                        <li class="menu_list"><a href="#">개인정보 설정</a></li>
                        <li class="menu_list"><a href="#">암호설정</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->