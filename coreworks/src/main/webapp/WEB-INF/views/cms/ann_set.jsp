<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/cms_menu.jsp" />
    
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>연차 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       <h3>연차 생성 조건</h3>
                       	<div class="main_cnt_list clearfix list2">
                       			<div class="descr">
                       				 <label>※ &nbsp;연차는 한 번 생성하면 수정이 불가능하니 신중하게 설정하시기 바랍니다.</label>
                       			</div>
								<ul class="list">
									<li>하단 툴을 활용해 결재 문서 본문의 폰트, 정렬, 이미지 첨부, 표 삽입 등을 사용할 수 있습니다</li>
									<li>디자인을 통한 고급 편집 기능 외 HTML/TEXT 기능도 사용할 수 있습니다.</li>
									<li>MS OFFICE나 한글 파일을 복사해 에디터에 붙여 넣기를 하면 원본 파일 서식과 100% 일치하지 않을 수 있습니다. 그럴 때는 사용자가 에디터의 소스 보기를 통해 수정을 해주시거나 에디터에서 직접 작성해 주셔야 합니다.</li>
								</ul>
							
							</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                
                                
                                <tr>
                                    <td class="tit1">생성년도</td>
                                    <td class="tit tbselect">
                                    <select name="" id="">
                           		    <option value="">2020</option>
                            		<option value="">2021</option>
                       				</select>
                       		 		</td>                           
                                </tr>
                                <tr>
                                    <td class="tit2_qwe"><a href="#">연차일수</a></td>
                                    <td class="tbdesc" >
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								입사년도/1년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								1년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                                    		<div class="tbdiv">
                							<div class="tbdiv2">
                								2년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								3년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								4년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								6년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>
                						<div class="tbdiv">
                							<div class="tbdiv2">
                								7년차
                							</div>
                							<div>
                								생성
                							</div>              											
                						</div>				
	                                    <div class="tbdiv">
	                							<div class="tbdiv2">
	                								10년차
	                							</div>
	                							<div>
	                								생성
	                							</div>              											
	                						</div>
                                    
                                   	
                                </tr>
                            
                                <tr >
                                    <td class="tit1 tit3"rowspan="3"><a href="#">1년차 연차 자동생성</a></td>
                                    <td class="tit tbauto">* 계속하여 근로한 기간이 1년 미만인 근로자에게 입사일을 기준으로 1개월에 1일의 연차를 생성하며,<br>
													 1년이 되었을 때 직전 회계연도 기준 근무일을 일할 계산하여 연차를 지급합니다. </td>                 
                                </tr>
                                <tr class="tbbot">
                                	<td class="tit tbauto"><input type="radio" name="form" id="use" value="Y"><label for="use">사용함</label></td>                   
                                </tr>
                                <tr class="tbbot">
                                	<td class="tit tbauto"><input type="radio" name="form" id="notuse" value="N"><label for="notuse">사용안함</label></td>                   
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                 	               
            <div class="annbtn">
            <a href="" class="button btn_blue add">저장</a>
            </div>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
 
<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });
</script>
</body>
</html>