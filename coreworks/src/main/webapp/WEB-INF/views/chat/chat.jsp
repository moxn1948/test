<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/chat.css">

<div class="open-button">
	<i class="far fa-comment-alt chaticon"></i>
</div>

<jsp:include page="chat_pop_list.jsp"/>
<jsp:include page="chat_pop_room.jsp"/>

<script>
<%-- 채팅 열고 닫기 이벤트 --%>
$(".open-button").click(function() { 
	var dis = $(".chat").css("display");
	
	if(dis === "block") {
		$(".chat").css("display","none");
	} else if (dis === "none") {
		$(".chat").css("display","block");
	}
});

<%-- esc로 채팅 닫기 이벤트 --%>
$(document).on("keydown", "body", function(e) {
	if(e.keyCode === 27) {
		if($("#filebox_pop").css("display") == "inline-block") {
			$("#filebox_pop").css("display","none");
		} else if($("#chatForm").css("display") == "block") {
			$("#chatForm").css("display","none");
		} else if($(".chat").css("display") == "block") {
			$(".chat").css("display","none");
		}
	}
});
</script>